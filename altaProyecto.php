<!DOCTYPE html>
<!--
Página para dar de alta proyectos de alumnos
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        require_once 'bbdd.php';
        ?>
        <form method="POST">
            <p>Nombre del proyecto: <input type="text" name="nombre" required></p>
            <p>Fecha: <input type="date" name="fecha" required></p>
            <p>Nota: <input type="number" name="nota" min="0" max="10"></p>
            <p>Estudiante: <select name="estudiante">
                <?php
                $alumnos = selectCodeAlumnos();
                while ($fila = mysqli_fetch_assoc($alumnos)) {
                    echo "<option>";
                    echo $fila["code"];
                    echo "</option>";
                }
                ?>
                </select>
            </p>
            <p><input type="submit" name="boton" value="alta"></p>
        </form>
        <?php
        if (isset($_POST["boton"])) {
            // Recogemos los datos del formulario
            $nombre = $_POST["nombre"];
            $fecha = $_POST["fecha"];
            $nota = $_POST["nota"];
            $alumno = $_POST["estudiante"];
            $result = insertarProyecto($nombre, $fecha, $nota, $alumno);
            if ($result == "ok") {
                echo "Proyecto registrado";
            } else {
                echo "ERROR: $resultado";
            }
        }
        ?>
    </body>
</html>
