<!DOCTYPE html>
<!--
 Primer ejemplo de aplicación CRUD con BBDD 
 Este fichero va a ser el menú principal
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Aplicación con BBDD School</title>
    </head>
    <body>

        <h1>Stucom, la aplicación de gestión de alumnos</h1>
        <p><a href="alta.php">Registrar alumnos</a></p>
        <p><a href="altaProyecto.php">Registrar Proyectos</a></p>
        <p><a href="modificarEdad.php">Modificar edad de un alumno</a></p>
        <p><a href="listado.php">Listado de alumnos</a></p>
        <p><a href="edadStudents.php">Alumnos mayores de una edad determinada</a>
        <p><a href="borrarStudent.php">Borrar un alumno</a>
    </body>
</html>
