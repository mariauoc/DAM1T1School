<!DOCTYPE html>
<!--
Página que muestra los datos de los alumnos que tengan una edad mayor o igual a la indicada por el usuario
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Alumnos x edad</title>
    </head>
    <body>
        <form method="POST">
            Introduce la edad a consultar: <input type="number" name="edad" required>
            <br>
            <input type="submit" name="boton" value="Enviar">
        </form>
        <?php
        // si se ha pulsado ya el botón
        if (isset($_POST["boton"])) {
            // recogemos la edad introducida por el usuario
            $edad = $_POST["edad"];
            // incluimos fichero donde tenermos la función que realiza la consulta
            require_once 'bbdd.php';
            // Llamamos a la consulta pasando la edad que hemos recogido del formulario
            $alumnos = selectAlumnosByEdad($edad);
            // Mostramos el resultado
            echo "<table>";
            echo "<tr>";
            echo "<th>Código</th><th>Nombre</th><th>Apellidos</th><th>Edad</th><th>Género</th>";
            echo "</tr>";
            // Lo vamos mostrando mientras haya filas en el resultado
            while ($fila = mysqli_fetch_assoc($alumnos)) {
                echo "<tr>";
                // Mostramos los datos del alumno actual ($fila)
                foreach ($fila as $dato) {
                    echo "<td>$dato</td>";
                }
                echo "</tr>";
            }
            echo "</table>";
        }
        ?>
        
          <p><a href="index.php">Volver al menu principal</a></p>
    </body>
</html>
