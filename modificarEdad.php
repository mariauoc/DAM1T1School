<!DOCTYPE html>
<!--
  Modificar edad
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Modificar Edad</title>
    </head>
    <body>
        <?php
        require_once 'bbdd.php';
        ?>
        <form method="post">
            <p>Escoge el alumno al que quieres modificar:
                <select name="alumno">
                    <?php
                    $alumnos = selectCodeAlumnos();
                    while ($fila = mysqli_fetch_assoc($alumnos)) {
                        echo "<option>";
                        echo $fila["code"];
                        echo "</option>";
                    }
                    ?>
                </select>
            </p>
            <p>Nueva edad: <input type="number" name="edad"></p>
            <p><input type="submit" name="boton" value="Modificar"></p>
        </form>
        <?php
        if (isset($_POST["boton"])) {
            $alumno = $_POST["alumno"];
            $nuevaEdad = $_POST["edad"];
            $result = updateEdadAlumno($alumno, $nuevaEdad);
            if ($result == "ok") {
                echo "Edad del alumno modificada";
            } else {
                echo "ERROR: $result";
            }
        }
        ?>
    </body>
</html>
