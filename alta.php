<!DOCTYPE html>
<!--
alta alumno
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Registrar alumnos</title>
    </head>
    <body>
        <h2>Datos del alumno</h2>
        <form action="" method="POST">
            Código: <input type="number" name="codigo" required><br>
            Nombre: <input type="text" name="nombre" required ><br>
            Apellidos: <input type="text" name="apellidos" required><br>
            Edad: <input type="number" name="edad" required=""><br>
            Género: <input type="radio" name="genero" value="Hombre" required> Hombre
            <input type="radio" name="genero" value="Mujer" required> Mujer
            <br>
            <input type="submit" value="Registrar" name="boton" >
        </form>
        <?php
        // Si han pulsado el botón
        if (isset($_POST["boton"])) {
            // Recogemos los datos del formulario
            $codigo = $_POST["codigo"];
            $nombre = $_POST["nombre"];
            $apellidos = $_POST["apellidos"];
            $edad = $_POST["edad"];
            $genero = $_POST["genero"];
            // incluimos el fichero bbdd.php para poder usar sus funciones
            require_once 'bbdd.php';
            // insertamos los datos en la bbdd
            $resultado = insertar_alumno($codigo, $nombre, $apellidos, $edad, $genero);
            if ($resultado == "ok") {
                echo "Alumno dado de alta.";
            } else {
                echo "ERROR: $resultado";
            }
        }
        ?>
        <p><a href="index.php">Volver al menu principal</a></p>
    </body>
</html>
