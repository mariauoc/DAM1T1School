<!DOCTYPE html>
<!--
Página que muestra los datos de los alumnos
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Alumnos</title>
    </head>
    <body>
        <?php
        // incluimos el fichero para poder usar las funciones
        require_once 'bbdd.php';
        // Llamamos a la consulta que queremos y cogemos el resultado
        $alumnos = selectAllAlumnos();
        echo "<table>";
        echo "<tr>";
        echo "<th>Código</th><th>Nombre</th><th>Apellidos</th><th>Edad</th><th>Género</th>";
        echo "</tr>";
        // Lo vamos mostrando mientras haya filas en el resultado
        while ($fila = mysqli_fetch_assoc($alumnos)) {
            echo "<tr>";
            // Mostramos los datos del alumno actual ($fila)
            foreach ($fila as $dato) {
                echo "<td>$dato</td>";
            }
            echo "</tr>";
        }
        echo "</table>";
        ?>

        <p><a href="index.php">Volver al menu principal</a></p>
    </body>
</html>
