<!DOCTYPE html>
<!--
Página que permite seleccionar un alumno para borrarlo
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Borrar alumno</title>
    </head>
    <body>
        <form method="POST">
            Selecciona el código del alumno que quieres borrar:
            <select name="alumno">
                <?php
                // Incluimos fichero de funciones
                require_once 'bbdd.php';
                // Traemos todos los códigos de los alumnos
                $codigos = selectCodeAlumnos();
                // mostramos los códigos dentro de cada option del select
                while ($fila = mysqli_fetch_assoc($codigos)) {
                    echo "<option>";
                    echo $fila["code"];
                    echo "</option>";
                }
                ?>
            </select>
            <input type="submit" value="borrar" name="boton">
        </form>
        <?php
        if (isset($_POST["boton"])) {
            $codigo = $_POST["alumno"];
            require_once 'bbdd.php';
            $result = borrarAlumno($codigo);
            if ($result == "ok") {
                echo "Alumno borrado con éxito";
            } else {
                echo "ERROR: $result";
            }
        }
        ?>
          <p><a href="index.php">Volver al menu principal</a></p>
    </body>
</html>
