<?php

/* 
 * Fichero que incluirá todas las funciones que tengan que ver con la bbdd
 */

// Función que modifica la edad de un alumno
function updateEdadAlumno($alumno, $nuevaEdad) {
    $c = conectar();
    $update = "update student set age=$nuevaEdad where code=$alumno";
    if (mysqli_query($c, $update)) {
        $resultado = "ok";
    } else {
        $resultado = mysqli_error($c);
    }
    desconectar($c);
    return $resultado;
}

// Función que inserta un proyecto en la base de datos
function insertarProyecto($nombre, $fecha, $nota, $alumno) {
    $c = conectar();
    $insert = "insert into project values (null, '$nombre', '$fecha', $nota, $alumno)";
    if (mysqli_query($c, $insert)) {
        $resultado = "ok";
    } else {
        $resultado = mysqli_error($c);
    }
    desconectar($c);
    return $resultado;
}

// Función que borra un alumno a partir del código en la base de datos
function borrarAlumno($codigo) {
    $c = conectar();
    $delete = "delete from student where code=$codigo";
    if (mysqli_query($c, $delete)) {
        $resultado = "ok";
    } else {
        $resultado = mysqli_error($c);
    }
    desconectar($c);
    return $resultado;
}

// Función que devuelve todos los códigos de los alumnos
function selectCodeAlumnos() {
    $c = conectar();
    $select = "select code from student";
    $result = mysqli_query($c, $select);
    desconectar($c);
    return $result;
}


// Función que devuelve todos los datos de los alumnos mayores o iguales
// a una edad pasada como parámetro
function selectAlumnosByEdad($edad) {
    $c = conectar();
    $select = "select * from student where age >= $edad";
    $resultado = mysqli_query($c, $select);
    desconectar($c);
    return $resultado;
}


// Función que devuelve todos los datos de todos los alumnos de la bbdd
function selectAllAlumnos() {
   // Conectamos a la bbdd
    $c = conectar();
    // Preparamos la consulta que queremos realizar
    $select = "select * from student";
    // Ejecutamos la consulta y RECOGEMOS el resultado
    $resultado = mysqli_query($c, $select);
    // desconectamos
    desconectar($c);
    // Devolvemos el resultado
    return $resultado;
}


// Función que inserta un alumno en la bbdd
function insertar_alumno($codigo, $nombre, $apellidos, $edad, $genero) {
    // Conectamos a la bbdd
    $c = conectar();
    // Preparamos la consulta que queremos ejecutar en una variable
    $insert = "insert into student values ($codigo, '$nombre', '$apellidos', 
            $edad, '$genero')";
    // Ejecutamos la consulta
    if (mysqli_query($c, $insert)) {
        // Si ha ido bien devolveremos ok
        $resultado = "ok";
    } else {
        // Si no ha ido bien devolvemos el mensaje de error de la bbdd
        $resultado = mysqli_error($c);
    }
    // Cerramos la conexión
    desconectar($c);
    // devolvemos el resultado
    return $resultado;
}

// Función que cierra una conexión
function desconectar($conexion) {
    mysqli_close($conexion);
}

// Función que se conecta a una base de datos (school)
function conectar() {
    // conectamos a la bbdd (nos devuelve una conexión)
    $conexion = mysqli_connect("localhost", "root", "root", "school");
    // si no ha podido conectar devuelve null, comprobamos
    if (!$conexion) {
        // Acabamos el programa dando msg de error
        die("No se ha podido establecer la conexión con el servidor");
    }
    // si todo ha ido ok devolvemos la conexión
    return $conexion;
}

